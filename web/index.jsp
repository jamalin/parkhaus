<%--
  Created by IntelliJ IDEA.
  User: Anita
  Date: 06.06.2019
  Time: 14:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Willkommen in der coolen Garage</title>
  <style>
    body {
      font-family: Arial, Helvetica;
    }
    .button {
      text-decoration: none;
      color: white;
      background-color: #899BC1;
      padding: 5px;
      border: solid 1px #525D73;
    }
    h1, h3 {
      color: #414A5C;
    }
  </style>
</head>
<body>
<div>
  <h1>Parkhaus</h1>
  <a href="CustomerServlet?homepage=true" class="button">Kundenansicht</a>
  <a href="CheckOutServlet" class="button">Kasse</a>
  <a href="AdminServlet" class="button">Adminseite</a>
</div>

</body>
</html>
