import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CustomerServlet")
public class CustomerServlet extends HttpServlet {

    private CarParkStatus carParkStatus;
    public CustomerServlet() {
        carParkStatus = CarParkStatus.getInstance();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebWriter writer = new WebWriter(response.getWriter());
        String typeInput = request.getParameter("type");
        String homepageInput = request.getParameter("homepage");

        int input  = -1;
        if (typeInput != null) {
            input = Integer.parseInt(typeInput);
        }
        if (homepageInput != null && homepageInput.equals("true")){
            showHomepage(writer);
            return;
        }

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");

        String[] outputParameters;
        

        switch (input) {
            case 0: {
                outputParameters = sellStandardParkingSpot();
                break;
            }
            case 1: {
                outputParameters = sellHandicappedParkingSpot();
                break;
            }
            case 2: {
                outputParameters = sellWomenParkingSpot();
                break;
            }
            case 3: {
                outputParameters = sellBikeParkingSpot();
                break;
            }
            case 4: {
                outputParameters = sellPkwParkingSpot();
                break;
            }
            case 5: {
                outputParameters = sellPickUpParkingSpot();
                break;
            }
            case 6: {
                outputParameters = sellSuvParkingSpot();
                break;
            }
            case 7: {
                outputParameters = sellTrikeParkingSpot();
                break;
            }
            case 8: {
                outputParameters = sellQuadParkingSpot();
                break;
            }
            default: {
                showErrorPage(writer);
                return;
            }
        }


        String[] body = {
                "Ihnen wurde der " + outputParameters[1] + " mit der Nummer: " + outputParameters[0] + " zugewiesen.<br>",
                "<a href=\"CustomerServlet?homepage=true\" class=\"button no-block\"> Okay </a>"
        };
        writer.write(
                "Zuweisung " + outputParameters[1],
                body
        );

    }

    private String[] sellStandardParkingSpot() {
        String[] out = new String[2];
        out[0] = carParkStatus.occupyStandardSpot();    //number
        out[1] = "Parkplatz";
        return out;
    }

    private String[] sellHandicappedParkingSpot() {
        String[] out = new String[2];
        out[0] = carParkStatus.occupyHandicappedSpot();
        out[1] = "Behindertenparkplatz";
        return out;
    }

    private String[] sellWomenParkingSpot() {
        String[] out = new String[2];
        out[0] = carParkStatus.occupyWomenSpot();
        out[1] = "Frauenparkpletz";
        return out;
    }

    private String[] sellBikeParkingSpot() {
        String[] out = new String[2];
        out[0] = carParkStatus.occupyBikeSpot();
        out[1] = "Motorradstellplatz";
        return out;
    }

    private String[] sellPkwParkingSpot() {
        String[] out = new String[2];
        out[0] = carParkStatus.occupyPkwSpot();
        out[1] = "Pkwstellplatz";
        return out;
    }
    private String[] sellSuvParkingSpot() {
        String[] out = new String[2];
        out[0] = carParkStatus.occupySuvSpot();
        out[1] = "Suvstellplatz";
        return out;
    }
    private String[] sellPickUpParkingSpot() {
        String[] out = new String[2];
        out[0] = carParkStatus.occupyPickUpSpot();
        out[1] = "PickUpstellplatz";
        return out;
    }
    private String[] sellTrikeParkingSpot() {
        String[] out = new String[2];
        out[0] = carParkStatus.occupyTrikeSpot();
        out[1] = "Trikestellplatz";
        return out;
    }
    private String[] sellQuadParkingSpot() {
        String[] out = new String[2];
        out[0] = carParkStatus.occupyQuadSpot();
        out[1] = "Quadstellplatz";
        return out;
    }

    private void showErrorPage(WebWriter writer) {
        String[] body = {
                "Ein ungueltiger Get-Parameter wurde gefunden. Das haette nicht passieren sollen."
        };
        writer.write(
                "Huch!",
                body
        );
    }

    private void showHomepage(WebWriter writer) {
        int freeStandardSpots = carParkStatus.getFreeStandardSpots();
        int freeHandicappedSpots = carParkStatus.getFreeHandicappedSpots();
        int freeWomenSpots = carParkStatus.getFreeWomenSpots();
        int freeBikeSpots = carParkStatus.getFreeBikeSpots();
        int freePkwSpots = carParkStatus.getFreePkwSpots();
        int freePickUpSpots = carParkStatus.getFreePickUpSpots();
        int freeSuvSpots = carParkStatus.getFreeSuvSpots();
        int freeTrikeSpots = carParkStatus.getFreeTrikeSpots();
        int freeQuadSpots = carParkStatus.getFreeQuadSpots();

        String standardSpotsAvailable = freeStandardSpots == 0 ? "disabled" : "";
        String handicappedSpotsAvailable = freeHandicappedSpots == 0 ? "disabled" : "";
        String womenSpotsAvailable = freeWomenSpots == 0 ? "disabled" : "";
        String bikeSpotsAvailable = freeBikeSpots == 0 ? "disabled" : "";
        String pkwSpotsAvailable = freePkwSpots == 0 ? "disabled" : "";
        String pickUpSpotsAvailable = freePickUpSpots == 0 ? "disabled" : "";
        String suvSpotsAvailable = freeSuvSpots == 0 ? "disabled" : "";
        String trikeSpotsAvailable = freeTrikeSpots == 0 ? "disabled" : "";
        String quadSpotsAvailable = freeQuadSpots == 0 ? "disabled" : "";


        String[] body = {
                "<h1> Wilkommen bei CooleGarage! </h1>",
                "<h3> Bitte wahle einen Parkplatztyp. </h3>",
                "<div class=\"button-container\">",
                "<a href=\"CustomerServlet?type=0\" class=\"button " + standardSpotsAvailable + " \">Standardparkplatz (" + freeStandardSpots + " freie Plaetze)</a><br>",
                "<a href=\"CustomerServlet?type=1\" class=\"button " + handicappedSpotsAvailable + " \">Behindertenparkplatz (" + freeHandicappedSpots + " freie Plaetze)</a><br>",
                "<a href=\"CustomerServlet?type=2\" class=\"button " + womenSpotsAvailable + " \">Frauenparkplatz (" + freeWomenSpots + " freie Plaetze)</a><br>",
                "<a href=\"CustomerServlet?type=3\" class=\"button " + bikeSpotsAvailable + " \">Motorradstellplatz (" + freeBikeSpots + " freie Plaetze)</a><br>",
                "<a href=\"CustomerServlet?type=4\" class=\"button " + pkwSpotsAvailable + " \">Pkwstellplatz (" + freePkwSpots + " freie Plaetze)</a><br>",
                "<a href=\"CustomerServlet?type=5\" class=\"button " + pickUpSpotsAvailable + " \">PickUpstellplatz (" + freePickUpSpots + " freie Plaetze)</a><br>",
                "<a href=\"CustomerServlet?type=6\" class=\"button " + suvSpotsAvailable + " \">Suvstellplatz (" + freeSuvSpots + " freie Plaetze)</a><br>",
                "<a href=\"CustomerServlet?type=7\" class=\"button " + trikeSpotsAvailable + " \">Trikestellplatz (" + freeTrikeSpots + " freie Plaetze)</a><br>",
                "<a href=\"CustomerServlet?type=8\" class=\"button " + quadSpotsAvailable + " \">Quadstellplatz (" + freeQuadSpots + " freie Plaetze)</a><br>",
                "<div>"
        };
        writer.write(
                "Willkommen!",
                body
        );
    }

}
