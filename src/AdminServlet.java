import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import static java.util.Arrays.asList;

@WebServlet(name = "AdminServlet")
public class AdminServlet extends HttpServlet {

    private CarParkStatus carParkStatus;
    public AdminServlet() {
        carParkStatus = CarParkStatus.getInstance();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String loadDataInput = request.getParameter("data");

        if (loadDataInput != null) {
            if (!loadDataInput.equals("clean") && !loadDataInput.equals("init")) {
                throw new IllegalArgumentException("Ungueltigen Get-Parameter erhalten");
            }
            loadData(loadDataInput);
        }
        WebWriter writer = new WebWriter(response.getWriter());

        ParkingSpot[] standardSpots = carParkStatus.getStandardSpots().getSpots();
        ParkingSpot[] handicappedSpots = carParkStatus.getHandicappedSpots().getSpots();
        ParkingSpot[] womenSpots = carParkStatus.getWomenSpots().getSpots();
        ParkingSpot[] bikeSpots = carParkStatus.getBikeSpots().getSpots();
        ParkingSpot[] pkwSpots = carParkStatus.getPkwSpots().getSpots();
        ParkingSpot[] suvSpots = carParkStatus.getSuvSpots().getSpots();
        ParkingSpot[] PickUpSpots = carParkStatus.getPickUpSpots().getSpots();
        ParkingSpot[] trikeSpots = carParkStatus.getTrikeSpots().getSpots();
        ParkingSpot[] quadSpots = carParkStatus.getQuadSpots().getSpots();


        ArrayList<String> output = new ArrayList<>();

        output.add("<br><a href=\"AdminServlet?data=clean\" class=\"button no-block\"> Daten loeschen </a>");
        output.add("<a href=\"AdminServlet?data=init\" class=\"button no-block\"> Daten initialisieren </a><br><br>");

        writeTable(output, "Standardparkplaetze", standardSpots, carParkStatus.getFreeStandardSpots());
        writeTable(output, "Behindertenparkplaetze", handicappedSpots, carParkStatus.getFreeHandicappedSpots());
        writeTable(output, "Frauenparkplaetze", womenSpots, carParkStatus.getFreeWomenSpots());
        writeTable(output, "Motorradstellplaetze", bikeSpots, carParkStatus.getFreeBikeSpots());
        writeTable(output, "Suvstellplaetze", suvSpots, carParkStatus.getFreeSuvSpots());
        writeTable(output, "PickUpstellplaetze", PickUpSpots, carParkStatus.getFreePickUpSpots());
        writeTable(output, "Pkwstellplaetze", pkwSpots, carParkStatus.getFreePkwSpots());
        writeTable(output, "Trikestellplaetze", trikeSpots, carParkStatus.getFreeTrikeSpots());
        writeTable(output, "Quadstellplaetze", quadSpots, carParkStatus.getFreeQuadSpots());


        writer.write(
                "Admin",
                output
        );

    }

    private void writeTable(ArrayList<String> output, String header, ParkingSpot[] spots, int freeSpots) {
        output.add("<h3>" + header + "</h3>");
        output.add("freie Parkplaetze: " + freeSpots + "<br>");
        output.add("Belegte Parkplaetze: <br>");
        output.add("<table><tr><th>Parkplatznummer</th><th>Belegt seit:</th></tr>");


        boolean spotsEmpty = true;
        for (ParkingSpot spot: spots) {
            if (spot.isOccupied()) {
                spotsEmpty= false;
                output.add("<tr><td>" + spot.getNumber() + "</td><td>" + spot.getOccupiedSince() + "</td></tr>");
            }
        }

        if (spotsEmpty) {
            output.add("<tr><td> Keine belegten Parkplaetze gefunden </td><td></td></tr>");
        }

        output.add("</table><br><br>");
    }

    private void loadData(String loadDataInput) {
        DataLoader loader = new DataLoader();
        if (loadDataInput.equals("clean")) {
            loader.cleanStatus();
        }
        if (loadDataInput.equals("init")) {
            loader.initCarParkStatus();
        }
    }

}
