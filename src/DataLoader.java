public class DataLoader {

    private CarParkStatus carParkStatus;

    public DataLoader() {
        carParkStatus = CarParkStatus.getInstance();
    }

    public void cleanStatus() {
        carParkStatus.setStandardSpots(new ParkingSpot[0]);
        carParkStatus.setHandicappedSpots(new ParkingSpot[0]);
        carParkStatus.setWomenSpots(new ParkingSpot[0]);
        carParkStatus.setBikeSpots(new ParkingSpot[0]);
        carParkStatus.setpkwSpots(new ParkingSpot[0]);
        carParkStatus.setSuvSpots(new ParkingSpot[0]);
        carParkStatus.setPickUpSpots(new ParkingSpot[0]);
        carParkStatus.setTrikeSpots(new ParkingSpot[0]);
        carParkStatus.setQuadSpots(new ParkingSpot[0]);

    }

    public void initCarParkStatus() {
        ParkingSpot[] standardSpots = new ParkingSpot[20];
        ParkingSpot[] handicappedSpots = new ParkingSpot[20];
        ParkingSpot[] womenSpots = new ParkingSpot[20];
        ParkingSpot[] bikeSpots = new ParkingSpot[20];
        ParkingSpot[] pkwSpots = new ParkingSpot[20];
        ParkingSpot[] suvSpots = new ParkingSpot[20];
        ParkingSpot[] pickUpSpots = new ParkingSpot[20];
        ParkingSpot[] trikeSpots = new ParkingSpot[20];
        ParkingSpot[] quadSpots = new ParkingSpot[20];

        for (int i = 0; i < 20; i++) {
            handicappedSpots[i] = new ParkingSpot(0, i);
            womenSpots[i] = new ParkingSpot(1, i);
            standardSpots[i] = new ParkingSpot(2, i);
            bikeSpots[i] = new ParkingSpot(3, i);
            pickUpSpots[i] = new ParkingSpot(4, i);
            pkwSpots[i] = new ParkingSpot(5, i);
            suvSpots[i] = new ParkingSpot(6, i);
            trikeSpots[i] = new ParkingSpot(7, i);
            quadSpots[i] = new ParkingSpot(8, i);
        }


        carParkStatus.getStandardSpots().init(standardSpots, 5, 2);
        carParkStatus.getHandicappedSpots().init(handicappedSpots, 7, 2);
        carParkStatus.getWomenSpots().init(womenSpots, 6, 2);
        carParkStatus.getPkwSpots().init(pkwSpots, 5, 2);
        carParkStatus.getSuvSpots().init(suvSpots, 6, 2.5);
        carParkStatus.getTrikeSpots().init(trikeSpots, 4.5, 1.5);
        carParkStatus.getBikeSpots().init(bikeSpots, 2, 1);
        carParkStatus.getPickUpSpots().init(pickUpSpots, 7, 3);
        carParkStatus.getQuadSpots().init(quadSpots, 4, 1.5);

    }
}
