import java.io.PrintWriter;
import java.util.ArrayList;

public class WebWriter {

    private PrintWriter writer;

    public WebWriter(PrintWriter writer) {
        this.writer = writer;
    }

    public void write(String title, String[] body) {
        writer.println("<!DOCTYPE html><html>");
        writer.println("<head>");
            writer.println("<meta charset=\"UTF-8\" />");
            writer.println("<title>" + title + "</title>");
            writer.println("<style>"
                    + "body { font-family: Arial, Helvetica; }"
                    + ".button {"
                        + "text-decoration: none;"
                        + "color: white;"
                        + "background-color: #899BC1;"
                        + "padding: 5px;"
                        + "border: solid 1px #525D73;"
                        + "display: block;"
                        + "margin-bottom: -7px;"
                    + "}"
                    + ".no-block { display: inline-block; }"
                    + "form { text-align: right; width: fit-content; }"
                    + "input { margin-bottom: 5px; }"
                    + ".button-container { width: fit-content; }"
                    + ".disabled { pointer-events: none; cursor: default; background-color: #909090; }"
                    + ".error { color: red; }"
                    + "h1, h3 { color: #414A5C; }"
                    + "</style>");
        writer.println("</head>");

        writer.println("<body>");
            writer.println("<a href=\"CustomerServlet?homepage=true\" class=\"button no-block\">Kundenansicht</a>");
            writer.println("<a href=\"CheckOutServlet\" class=\"button no-block\">Kasse</a>");
            writer.println("<a href=\"AdminServlet\" class=\"button no-block\">Adminseite</a><br><br>");
            for (String line : body) {
                writer.println(line);
            }
        writer.println("</body>");
        writer.println("</html>");

    }

    public void write(String title, ArrayList<String> body) {
        String[] result = body.toArray(new String[0]);
        this.write(title, result);
    }

}
